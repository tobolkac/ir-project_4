package ir.classifiers;

import java.util.*;


public class TestRocchio {
  /**
   * A driver method for testing the NaiveBayes classifier using
   * 10-fold cross validation.
   *
   * @param args a list of command-line arguments.  Specifying "-debug"
   *             will provide detailed output
   */
  public static void main(String args[]) throws Exception {
    String dirName = "/u/mooney/ir-code/corpora/yahoo-science/";
    String[] categories = {"bio", "chem", "phys"};
    System.out.println("Loading Examples from " + dirName + "...");
    List<Example> examples = new DirectoryExamplesConstructor(dirName, categories).getExamples();
    System.out.println("Initializing Rocchio classifier...");
    Rocchio r;
    boolean neg;
    // setting debug flag gives very detailed output, suitable for debugging
    if (args.length == 1 && args[0].equals("-neg"))
      neg = true;
    else
      neg = false;
    r = new Rocchio(categories, neg, examples);

    // Perform 10-fold cross validation to generate learning curve
    CVLearningCurve cvCurve = new CVLearningCurve(r, examples);
    cvCurve.run();
  }
}
