package ir.classifiers;

import java.io.*;
import java.util.*;

import ir.vsr.*;
import ir.utilities.*;


public class Rocchio extends Classifier {

  public static final String name = "Rocchio";

  public HashMap<Integer, HashMapVector> classWeights;

  boolean neg;

  int numCategories;
  
  List<Example> trainExamples = new ArrayList<Example>();
  
  InvertedIndex index;

  public Rocchio(String[] categories, boolean neg, List<Example> examples) {
    this.categories = categories;
    this.neg = neg;
    this.numCategories = categories.length;
    this.index = new InvertedIndex(examples);
  }

  /**
   * Returns the name
   */
  public String getName() 
  {
    return name;
  }
  
  public HashMapVector computeTFIDF(Example e, boolean toWeight)
  {
        HashMapVector d = e.getHashMapVector().copy();
        
        Map<String, TokenInfo> tokenHash = index.tokenHash;
        
        double maxtf = d.maxWeight();
        
        for(Map.Entry<String, Weight> entry : d.hashMap.entrySet())
        {
            TokenInfo t = tokenHash.get(entry.getKey());
            double weightedTF = (entry.getValue().getValue());
            Weight w = new Weight();
            if (toWeight)
                w.setValue( (weightedTF*(t.idf))/maxtf);
            else
                w.setValue( (weightedTF*(t.idf)));
            d.hashMap.put(entry.getKey(), w);
        }
        
        return d;
  }

  public void train(List<Example> trainingExamples)
  {
    
    classWeights = new HashMap<Integer, HashMapVector>();
    
    for(int i = 0; i<numCategories; i++)
    {
        classWeights.put(i, new HashMapVector());
    }
   
    for(Example e : trainingExamples)
    {
        HashMapVector p = classWeights.get(e.getCategory());
        
        HashMapVector d = computeTFIDF(e, true);
        
        p.addScaled(d,1);
        classWeights.put(e.getCategory(), p);
        if (neg)
        {
            for(Map.Entry<Integer, HashMapVector> en : classWeights.entrySet())
            {
                if (en.getKey() != e.getCategory())
                {
                    HashMapVector n = en.getValue();
                    n.addScaled(d, -1);
                    classWeights.put(en.getKey(), n);
                }
            }
        }
    }
  }

  public boolean test(Example testExample)
  {
    
    Map<String, TokenInfo> tokenHash = index.tokenHash;
    
    HashMapVector d = computeTFIDF(testExample, false);
    
    double maxCos = -2.0;
    int c = -1;
    if (classWeights.isEmpty())
    {
        c = 0;
    }
    else
    {
        for (int i = 0; i<numCategories; i++)
        {
          double s = d.cosineTo(classWeights.get(i));
          if (s > maxCos)
          {
            maxCos = s;
            c = i;
          }
        }
    }
    
    return (c == testExample.getCategory());
  }
}
