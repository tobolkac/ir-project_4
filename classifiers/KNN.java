package ir.classifiers;

import java.io.*;
import java.util.*;

import ir.vsr.*;
import ir.utilities.*;

public class KNN extends Classifier {

  public static final String name = "KNN";
  
  List<Example> trainExamples;
  
  InvertedIndex index;

  int k;

  int numCategories;

  public KNN(String[] categories, int k) {
    this.categories = categories;
    this.k = k;
    this.numCategories = categories.length;
  }

  /**
   * Returns the name
   */
  public String getName() 
  {
    return name;
  }

  public void train(List<Example> trainingExamples)
  {
    index = new InvertedIndex(trainingExamples);
    trainExamples = trainingExamples;
  }

  public boolean test(Example testExample)
  {
    Retrieval[] results = index.retrieve(testExample.getHashMapVector());
    
    double[] cats = new double[numCategories];
    int i = 1;
    for (Retrieval r: results)
    {
        if (i == k)
        {
            break;
        }
        int cat = findCategoryByName(r.docRef.toString());
       
        cats[cat] = cats[cat] + 1;
        i++;
    }
    
    return (argMax(cats) == testExample.getCategory());
  }
  
  
  public int findCategoryByName(String name)
  {
    for (Example ex : this.trainExamples)
    {
        if (name.equals(ex.getName()))
        {
            return ex.getCategory();
        }
    }
    return -1;
  }
}
